<?php
/**
 * Created by PhpStorm.
 * User: HectorOrlando
 * Date: 19/03/2019
 * Time: 12:28
 */

class Persona
{
    private $nombre;
    public function __construct($nombre)
    {
        $this->nombre = $nombre;
    }
    public function imprimir(){
        return $this->nombre;
    }
}

$empleado1 = new Persona('Orlando ');
echo $empleado1->imprimir();